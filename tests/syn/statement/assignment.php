//compiler 0
//
<?php
declare(strict_types=1);

$varA = 1;
$varB = 2.87;
$varC = "help";
$varD = null;
$varE = $varD;
$varF = ($varA + $varA);

return 0;