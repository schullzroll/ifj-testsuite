//compiler 2
//
<?php
declare(strict_types=1);

function fun (int $p) : int
{
    return 0;
}

$var = fun(5 6);

return 0;