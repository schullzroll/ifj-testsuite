//compiler 0
//
<?php
declare(strict_types=1);

function funA () : int
{
    return null;
}

function funB () : float
{
    return null;
}

function funC () : string
{
    return null;
}

function funD () : void
{}

return 0;