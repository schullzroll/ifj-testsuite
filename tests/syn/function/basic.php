//compiler 0
//
<?php
declare(strict_types=1);

function funA () : void
{}

function funB (int $p) : void
{}

function funC () : int
{}

function funD (int $p) : int
{}

return 0;