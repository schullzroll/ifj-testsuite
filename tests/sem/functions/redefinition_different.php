//compiler 3
//
<?php
declare(strict_types=1);

function fun () : int
{
    return 0;
}

function fun(int $p) : void
{}

fun(1);

return 0;